<h1>Description</h1>

The RichTextBox is a QControl and requires the QControl Toolkit, http://bit.ly/QControlNITools.  It inherits from and extends the String control.  It implements formatting through an HTML-like markdown.

There are two classes in this implementation:
- RichTextBox implements the markdown
- RichTextBoxWithToolbar implements the toolbar functionality

<h1>LabVIEW Version</h1>

This code is currently published in LabVIEW 2018

<h1>Build Instructions</h1>

The <b>RichTextBox.lvclass</b> and all its members can be distributed as a Class Library and built into the using application.

The <b>RichTextBoxWithToolbar.lvclass</b> and all its members can be distributed as a Class Library and built into the using application but requires the <b>RichTextBox.lvclass</b> and all its members as well.

<h1>Installation Guide</h1>

The <b>RichTextBox.lvclass</b> and all its members can be distributed as a Class Library and built into the using application.

The <b>RichTextBoxWithToolbar.lvclass</b> and all its members can be distributed as a Class Library and built into the using application but requires the <b>RichTextBox.lvclass</b> and all its members as well.

<h1>Execution</h1>
See documentation distributed with the QControl Toolkit.

<h1>Support</h1>
Submit Issues or Merge Requests through GitLab.
